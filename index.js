const express = require("express");
const http = require("http");
const cors = require("cors");
const app = express();
const server = http.createServer(app);
const io = require("socket.io")(server);
app.use(cors);
const registeredUsers = [];
io.on("connection", function(socket) {
  socket.on("register", info => {
    console.log("info: ", info);
    info.socket_id = socket.id;
    registeredUsers[info.udid] = info;
    console.log("[registered users] : ", registeredUsers);
  });
  socket.on("alert", function(posInfo) {
    console.log("[ALERT POS]", posInfo);
    io.emit("student:alert", posInfo);
  });

  socket.on("get:location", function(value0) {});
  socket.on("disconnect", function() {
    const socketIndex = getSocketIndx(socket.id);
    if (-1 !== socketIndex) {
      registeredUsers.splice(socketIndex, 1);
    }
  });
  function getSocketIndx(socketId) {
    if (Array.isArray(registeredUsers) && registeredUsers.length > 0) {
      const registeredUsersIds = registeredUsers.map(user => user.socket_id);
      return registeredUsersIds.indexOf(socketId);
    }
    return -1;
  }
});
const port = process.env.PORT || 3000;
app.get("/", (req, res) => {
  res.send("SPECIALERT");
});
server.listen(port, () => console.log(`Listening on port ${port}`));
